function BubbleChart(chart) {
	const FPS = 60,
		percentRegex = /\d+\%/,
		elHeader =
			'<svg class="header" viewBox="0 0 100 100">' +
			'<path id="p" d="M10,50 a20,20 0 0,1 80,0" fill="transparent" />' +
			'<text x="5" y="50" text-anchor="middle"><textPath xlink:href="#p" side="left" startOffset="50%">[%header%]</textPath></text>' +
			"</svg>";

	let ndID = 0,
		nodes = {},
		chartAR = chart.width / chart.height,
		elContainer = document.querySelector(chart.container),
		stage = {},
		mouseX,
		mouseY;

	// animation vars
	let anim, interval, now, then, delta;

	createStage();

	if (chart.animation && !chart.animation.disable) {
		anim = chart.animation;
		let fps = anim.fps || FPS,
			dFps = FPS / fps;
		interval = 1000 / fps;
		anim.vMax *= dFps;
		anim.vMin *= dFps;
	}

	initChart();
	initNodes(chart.nodes, stage);
	initLinks(chart.links);
	this.nodes = nodes;
	resize();
	window.addEventListener("resize", resize);

	if (anim || chart.mouse) {
		if (chart.mouse) elContainer.addEventListener("mousemove", mousemove);
		then = Date.now();
		render();
	}

	function render() {
		requestAnimationFrame(render);

		// throtle to fps
		now = Date.now();
		delta = now - then;
		if (delta < interval) return;
		then = now - (delta % interval);

		let id, node, dx, dy, d, force, forceX, forceY;
		let locked = false;
		for (id in nodes) {
			node = nodes[id];
			locked = false;

			if (anim) {
				node.anim.x += node.anim.vx;
				node.anim.y += node.anim.vy;
				node.x += Math.sin(node.anim.x) * anim.vAmp;
				node.y += Math.sin(node.anim.y) * anim.vAmp;
			}

			// Add central force and viscous damping force
			forceX = (node.cx - node.x) * anim.stiffness - anim.damping * node.vx;
			forceY = (node.cy - node.y) * anim.stiffness - anim.damping * node.vy;

			// add mouse force if close enough
			if (chart.mouse) {
				dx = mouseX - node.cx;
				dy = mouseY - node.cy;
				d = Math.sqrt(dx * dx + dy * dy);
				if (d < node.grab) {
					locked = true;
				} else if (d < node.field) {
					force = (chart.mouse.force * node.r) / d;
					forceX += (force * dx) / d;
					forceY += (force * dy) / d;
				}
			}

			if (locked) {
				node.vx = 0;
				node.vy = 0;
				node.x = mouseX;
				node.y = mouseY;
			} else {
				node.vx += forceX;
				node.vy += forceY;
				node.x += node.vx;
				node.y += node.vy;
			}
		}
		renderNodes();
		renderLinks();
	}

	function resize() {
		let bb = stage.content.getBoundingClientRect(),
			containerAR = bb.width / bb.height;
		if (containerAR > chartAR) {
			stage.scale = bb.height / chart.height;
			stage.Y = 0;
			stage.X = (bb.width - stage.scale * chart.width) / 2;
		} else {
			stage.scale = bb.width / chart.width;
			stage.X = 0;
			stage.Y = (bb.height - stage.scale * chart.height) / 2;
		}
		renderStage();
		renderNodes();
		renderLinks();
	}

	function createStage() {
		stage.content = createDiv(elContainer, "chartBox");
		stage.el = createDiv(stage.content, "bubbleChart");
		setSize(stage.el, chart.width, chart.height);
		stage.content.style.background = chart.background;
	}

	function initNodes(list, parent, sateliter) {
		list.forEach(node => initNode(node, parent, sateliter));
	}
	function initNode(node, parent, sateliter) {
		if (!node.id) node.id = ndID++;
		if (anim) {
			node.anim = {
				x: 0,
				y: 0,
				vx: rand(anim.vMin, anim.vMax),
				vy: rand(anim.vMin, anim.vMax)
			};
		}
		node.parent = parent;
		nodes[node.id] = node;
		node.type = node.type || chart.nodeType;
		node.x = node.x || 0;
		node.y = node.y || 0;
		node.vx = node.vy = 0;
		setVar(node, "r,x,y");

		if (sateliter) initSatelite(node, sateliter);
		setPercent(node, "x,y,r");
		node.cx = node.x;
		node.cy = node.y;
		if (chart.mouse) {
			node.field = node.r * chart.mouse.range;
			node.grab = node.r * chart.mouse.grab;
		}

		createNode(node);
		if (node.header) addHeader(node);
		if (node.title || node.subtile) createContent(node);
		if (node.internals) {
			node.internals.forEach(n => (n.r = n.r || chart.innerRadius));
			initNodes(node.internals, node);
		}
		if (node.satelites) {
			node.satelites.ref = node;
			if (typeof node.satelites.r == "string") node.satelites.r = chart.vars[node.satelites.r];

			initNodes(node.satelites.nodes, parent, node.satelites);
		}

		if (node.click) node.el.addEventListener("click", e => node.click(e, node));
		if (node.enter) node.el.addEventListener("mouseenter", e => node.enter(e, node));
		if (node.leave) node.el.addEventListener("mouseleave", e => node.leave(e, node));
	}

	function setVar(o, props) {
		props.split(",").forEach(p => {
			if (typeof o[p] == "string" && chart.vars[o[p]]) o[p] = chart.vars[o[p]];
		});
	}

	function initSatelite(node, sateliter) {
		node.sateliter = sateliter;
		node.r = node.r || sateliter.r;
		node.class = node.class || sateliter.class;
		let d = sateliter.ref.r + node.r + sateliter.length,
			angle = (node.angle * Math.PI) / 180;
		node.x = sateliter.ref.x + d * Math.cos(angle);
		node.y = sateliter.ref.y + d * Math.sin(angle);
		let c = {
			from: sateliter.ref.id,
			to: node.id
		};
		c.head = sateliter.head || chart.head;
		c.tail = sateliter.tail || chart.tail;
		chart.links.push(c);
	}

	function createNode(node) {
		if (node.url) {
			node.el = createEl("a", node.parent.el, "node");
			node.el.setAttribute("href", node.url);
		} else {
			node.el = createDiv(node.parent.el, "node");
		}
		node.el.classList.add(node.type);
		if (node.class) node.class.split(",").forEach(c => node.el.classList.add(c.trim()));
	}
	function renderNodes() {
		for (let nd in nodes) {
			let node = nodes[nd];
			if (node.xP) node.x = node.xP * node.parent.r * 2;
			if (node.yP) node.y = node.yP * node.parent.r * 2;
			if (node.rP) node.r = node.rP * node.parent.r;
			if (node.links && (node.x == 0 || node.y == 0)) {
				let x = 0,
					y = 0;
				node.linkedNodes.forEach(n => {
					x += n.x;
					y += n.y;
				});
				if (node.x == 0) node.x = x / node.links.length;
				if (node.y == 0) node.y = y / node.links.length;
				node.cx = node.x;
				node.cy = node.y;
			}
			drawNode(node);
		}
	}

	function initLinks(links) {
		links.forEach(link => initLink(link));
	}
	function initLink(link) {
		// endPoints
		link.head = link.head || chart.head;
		if (typeof link.head == "string") {
			link.head = chart.endpoints[link.head];
		}
		link.tail = link.tail || chart.tail;
		if (typeof link.tail == "string") {
			link.tail = chart.endpoints[link.tail];
		}

		let from = nodes[link.from],
			to = nodes[link.to];
		if (!from.links) {
			from.links = [];
			from.linkedNodes = [];
		}
		if (!to.links) {
			to.links = [];
			to.linkedNodes = [];
		}
		from.links.push(link);
		to.links.push(link);
		from.linkedNodes.push(to);
		to.linkedNodes.push(from);
		createLink(link);
	}

	function createLink(link) {
		link.el = createDiv(stage.el, "connector");
		if (link.class) link.class.split(",").forEach(c => link.el.classList.add(c.trim()));

		if (link.head) createEndPoint(link, "head");
		if (link.tail) createEndPoint(link, "tail");

		if (link.label) link.labelEl = createDiv(link.el, "label", link.label);
	}

	function createEndPoint(link, end) {
		let isHead = end == "head",
			ep = link[end],
			s = ep.size,
			color = ep.color || chart.linkColor,
			el = createDiv(link.el, end + "," + ep.type);
		if (ep.type == "circle") {
			setSize(el, s, s);
			if (color) el.style.backgroundColor = color;
			//setPos(el, (isHead ? -s : s) / 2, -s / 2);
		} else if (ep.type == "arrow") {
			if (color) el.style.borderColor = color;
		} else if (ep.type == "fillArrow") {
			if (color) el.style.borderBottomColor = color;
			//setPos(el, (isHead ? -s : s) / 2, -s / 2);
		}
		ep.el = el;
	}

	function renderLinks() {
		chart.links.forEach(link => renderLink(link));
	}
	function renderLink(link) {
		let from = nodes[link.from],
			to = nodes[link.to],
			d = dist(to.x, to.y, from.x, from.y),
			ratio = (from.r + chart.linkGap / 2) / d;

		link.len = d - (to.r + from.r) - chart.linkGap;
		link.angle = Math.atan((from.y - to.y) / (from.x - to.x));
		if (from.x >= to.x) link.angle += Math.PI;
		link.x = from.x + (to.x - from.x) * ratio;
		link.y = from.y + (to.y - from.y) * ratio;
		drawLink(link);
	}

	function drawLink(link) {
		link.el.style.width = link.len + "px";
		setPos(link.el, link.x, link.y, 1, link.angle + "rad");
		if (link.labelEl) {
			link.labelEl.style.transform = "translate3d(-50%,-50%,0) rotate(" + -link.angle + "rad)";
		}
	}

	function htmlToElement(html, text) {
		var template = document.createElement("template");
		html = html.trim(); // Never return a text node of whitespace as the result
		template.innerHTML = html.replace("[%header%]", text);
		return template.content.firstChild;
	}

	function createContent(node) {
		let content = createDiv(node.el, "content");
		if (node.title) addText(content, "h1", node.title);
		if (node.subtitle) addText(content, "h2", node.subtitle);
	}
	function addHeader(node) {
		node.el.appendChild(htmlToElement(elHeader, node.header));
	}
	function addText(parent, tag, text) {
		let el = document.createElement(tag);
		el.innerText = text;
		parent.appendChild(el);
	}

	function renderStage() {
		setPos(
			stage.el,
			stage.X - (chart.width * (1 - stage.scale)) / 2,
			stage.Y - (chart.height * (1 - stage.scale)) / 2,
			stage.scale
		);
	}
	function drawNode(node) {
		setSize(node.el, node.r * 2, node.r * 2);
		setPos(node.el, node.x - node.r, node.y - node.r);
	}

	function setSize(el, w, h) {
		el.style.width = w + "px";
		el.style.height = h + "px";
	}
	function setPos(el, x, y, s, r) {
		let t = "translate3d(" + x + "px," + y + "px,0)";
		if (s) t += " scale(" + s + ")";
		if (r) t += " rotate(" + r + ")";
		el.style.transform = t;
	}
	function createDiv(parent, cl, text) {
		return createEl("div", parent, cl, text);
	}
	function createEl(tag, parent, cl, text) {
		let d = document.createElement(tag);
		parent.appendChild(d);
		if (cl) d.classList.add(...cl.replace(" ", "").split(","));
		if (text) d.innerText = text;
		return d;
	}

	function dist(x1, y1, x2, y2) {
		let dx = x1 - x2,
			dy = y1 - y2;
		return Math.sqrt(dx * dx + dy * dy);
	}
	function setPercent(o, props) {
		props.split(",").forEach(p => {
			if (percentRegex.test(o[p])) o[p + "P"] = parseFloat(o[p]) / 100;
		});
	}
	function initChart() {
		let p, e;
		if (chart.endpoints) {
			for (e in chart.endpoints) {
				p = chart.endpoints[e];
				p.type = e;
			}
		}
	}

	function rand(min, max) {
		return min + Math.random() * (max - min);
	}
	function mousemove(e) {
		mouseX = (e.clientX - stage.X) / stage.scale;
		mouseY = (e.clientY - stage.Y) / stage.scale;
	}
}

export default BubbleChart;
