import "./css/main.scss";
import BubbleChart from "./BubbleChart";

let chart1 = {
	width: 2318,
	height: 1458,
	container: "#feadshipchart>div",
	background: "#061630",

	nodeType: "circle",

	innerRadius: "55%",

	vars: {
		small: 128,
		big: 336,
		baseY: 700
	},

	animation: {
		fps: 30,
		vMin: 0.012,
		vMax: 0.04,
		vAmp: 1.2,
		stiffness: 0.2,
		damping: 1.8
	},
	mouse: {
		force: 5,
		range: 1.3,
		grab: 0.27
	},

	// conector endpoints
	head: "circle",
	tail: "circle",
	linkGap: 40,
	linkColor: "#00547a",
	endpoints: {
		circle: {
			size: 12
		},
		arrow: {
			size: 10
		},
		fillArrow: {
			length: 20,
			width: 10
		}
	},

	// nodes (bubbles)
	nodes: [
		{
			id: "DeVriesGroup",
			x: 706,
			y: "baseY",
			r: "big",
			click: (e, n) => console.log(n),
			header: "DE VRIES GROUP",
			class: "b1",
			internals: [
				{
					x: "22%",
					y: "45%",
					title: "KONINKLIJKE DE VRIES SCHEEPSBOUW",
					subtitle: "AALSMEER",
					class: "b3"
				},
				{
					x: "75%",
					y: "68%",
					title: "DE VRIES SCHEEPSBOUW MAKKUM",
					subtitle: "MAKKUM",
					class: "b3,an"
				}
			],
			satelites: {
				head: 0,
				length: 72,
				r: "small",
				class: "b2",

				nodes: [
					{
						angle: 90,
						title: "STI ENGINEERING",
						subtitle: "HEILOO",
						enter: (e, n) => console.log(n),
						leave: (e, n) => console.log(n)
					},
					{
						angle: 135,
						title: "DE VLIJT PERSONEELS WERVING",
						subtitle: "AALSMEER",
						url: "https://www.feadship.nl/"
					},
					{
						angle: 180,
						title: "DE KLERK BINNENBOUW",
						subtitle: "MOORDRECHT"
					},
					{
						angle: 225,
						title: "SCHEEPSWERF SLOB",
						subtitle: "PARENDRECHT"
					},
					{
						angle: 270,
						title: "AKERBOOM YACHT EQUIPMENT",
						subtitle: "LEIDEN"
					}
				]
			}
		},
		{
			id: "RoyalVanLent",
			x: 1875,
			y: "baseY",
			r: "big",
			header: "ROYAL VAN LENT",
			class: "b1",

			internals: [
				{
					x: "22%",
					y: "45%",
					title: "ROYAL VAN LENT SHIPYARD",
					subtitle: "KAAG",
					class: "b3"
				},
				{
					x: "75%",
					y: "68%",
					title: "ROYAL VAN LENT SHIPYARD",
					subtitle: "AMSTERDAM",
					class: "b3,an"
				}
			],
			satelites: {
				head: 0,
				length: 72,
				nodes: [
					{
						angle: 90,
						r: "small",
						title: "VAN DER LOO YACHT INTERIORS",
						subtitle: "WADDINXVEEN",
						class: "b2"
					}
				]
			}
		},
		{
			id: "FEADSHIP",
			y: 291,
			r: 238,
			header: "FEADSHIP",
			class: "b1",
			internals: [
				{
					x: "22%",
					y: "45%",
					title: "FEADSHIP",
					subtitle: "HOLLAND",
					class: "b3"
				},
				{
					x: "75%",
					y: "68%",
					title: "FEADSHIP",
					subtitle: "AMERICA",
					class: "b3,an"
				}
			]
		},
		{
			r: 150,
			y: 957,
			id: "NavalArchitects",
			title: "DE VOOGT NAVAL ARCHITECTS",
			subtitle: "HAARLEM",
			class: "b1"
		},
		{
			r: 150,
			y: 1353,
			id: "FeadshipRefit",
			title: "FEADSHIP REFIT & SERVICES",
			subtitle: "HAARLEM",
			class: "b1"
		}
	],
	// connections between bubbles
	links: [
		{
			from: "DeVriesGroup",
			to: "FEADSHIP"
		},
		{
			from: "RoyalVanLent",
			to: "FEADSHIP"
		},
		{
			from: "DeVriesGroup",
			to: "NavalArchitects"
		},
		{
			from: "RoyalVanLent",
			to: "NavalArchitects"
		},
		{
			from: "DeVriesGroup",
			to: "FeadshipRefit"
		},
		{
			from: "RoyalVanLent",
			to: "FeadshipRefit"
		}
	]
};

new BubbleChart(chart1);
