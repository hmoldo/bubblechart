# BubbleChart for FEADSHIP website

A small BubbleChart javascript package in vanilla javascript.

Developing:

``` bash
git clone https://hmoldo@bitbucket.org/hmoldo/bubblechart.git
cd bubblechart
yarn install
yarn start
```

## General chart options

| property      | description                              |
| ------------- | ---------------------------------------- |
| width, height | chart viewport dimensions                |
| container     | container element for the chart          |
| background    | background color                         |
| nodeType      | default node type (not implemented)      |
| innerRadius   | default internal node radius             |
| linkGap       | default connectors space from nodes      |
| linkType      | default connector type (not implemented) |
| endpoints     | defaults endpoints properties            |
| animate       | use animations on mouse move             |

## Node

| property  | description                                            |
| --------- | ------------------------------------------------------ |
| id        | unique id                                              |
| x, y      | position (numeric or percent)                          |
| r         | radius                                                 |
| class     | css classes in comma separated string                  |
| header    | Circular text                                          |
| title     | title text                                             |
| subtitle  | subtitle text                                          |
| internals | nodes displayed inside this node                       |
| satelites | nodes connected to this node in a radial layout        |
| angle     | (satelite node) angle from the center of central node  |
| length    | (satelite node) connector length from the central node |
| click     | mouseclick callback                                    |
| enter     | mouseover callback                                     |
| leave     | mouseleave callback                                    |

## Connector

| property | description                           |
| -------- | ------------------------------------- |
| from     | id of the start node                  |
| to       | id of the start node                  |
| head     | head shape (string or object)         |
| tail     | tail shape                            |
| class    | css classes in comma separated string |
| color    | stroke color                          |

