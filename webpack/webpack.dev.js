const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	entry: {
		main: "./src/index.js"
	},
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].[chunkhash].js",
		libraryTarget: "umd"
	},
	devtool: "source-map",
	devServer: {
		open: true, // open browser
		port: 8081,
		host: "0.0.0.0", // enable access from local network
		contentBase: "./src"
	},
	module: {
		rules: [
			{
				// use babel to transpill es6 to es5
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader"
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader, // create the css file
					{
						loader: "css-loader", // resolves url() and @imports inside CSS
						options: {
							modules: false,
							sourceMap: true
						}
					},
					{
						// First we transform SASS to standard CSS
						loader: "sass-loader",
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				// static assets
				test: /\.(jpe?g|gif|png|ico|svg|woff|ttf|eot|wav|mp3)$/,
				use: "file-loader"
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "style.css"
		}),
		new HtmlWebpackPlugin({
			hash: false,
			title: "BubbleChart",
			template: "./src/index.html",
			filename: "index.html",
			dev: true
		})
	]
};
