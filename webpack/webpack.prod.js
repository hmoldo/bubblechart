const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;

// extracts css from javascript to separated css file
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const CleanWebpackPlugin = require("clean-webpack-plugin");

const dist = path.resolve(__dirname, "../dist");

module.exports = {
	resolve: {
		alias: {
			"@": path.resolve("./src")
		}
	},
	entry: {
		main: "./src/index.js"
	},
	output: {
		path: dist,
		filename: "[name].js"
	},
	// devtool: "source-map",
	module: {
		rules: [
			{
				// parses html files imports src attributes of images
				test: /\.(html)$/,
				use: "html-loader"
			},
			{
				// use babel to transpill es6 to es5
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader"
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader, // create the css file
					// "style-loader", // creates style nodes from JS strings
					"css-loader", // resolves url() and @imports inside CSS
					{
						loader: "postcss-loader", // Then we apply postCSS fixes like autoprefixer and minifying
						options: {
							ident: "postcss",
							plugins: [require("autoprefixer")({}), require("cssnano")({ preset: "default" })],
							minimize: true
						}
					},
					"sass-loader" // First we transform SASS to standard CSS
				]
			},

			{
				test: /\.(png|jpe?g|gif|svg|ico)$/,
				use: [
					{
						// Using file-loader too
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							outputPath: "imgs"
						}
					}
				]
			},
			{
				// Apply rule for fonts files
				test: /\.(woff|woff2|ttf|otf|eot)$/,
				use: [
					{
						// Using file-loader too
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							outputPath: "fonts"
						}
					}
				]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "BubbleChart",
			template: "./src/index.html",
			filename: "./index.html"
		}),
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[id].css"
		}),
		new CleanWebpackPlugin(dist, {
			root: process.cwd()
		})
		// new Visualizer()
		/*
		new WebpackMd5Hash()
		*/
	]
};
